Darf
====

Develop, test, run, and deploy Rust as a scripted language with full 
support for Git and Crates.io and sane caching.


Intended Use-cases
------------------

- Cases where a scripts makes sense; not a large project.
- Cases where you want something user-friendly.
- Cases where you don't want the extra overhead of a cargo directory. 


Features
--------

- **Caching:** Won't recompile unless source changes.
- **Dependencies:** Supports dependencies from Crates.io and git.
- **Development friendly:** Only caches what you want.
- **Valid rust:** File can be included in other projects without 
  modification. 
- Mostly **transparent**: See Caveats bellow.


Caveats
-------

- Makes liberal use of /dev/tty.
- Argument 0 is not the script, but the cached binary.
- Refuses to build under root; Will run as root if build is not necessary.


Thanks
------
- This hack was inspired by lambda's one-liner on hacker news: 
  https://news.ycombinator.com/item?id=9145279
- The Rust comunity at reddit.com




Usage
-----

1. Copy the darf blob - starting from the first line 
   `//bin/true/` up to and including the closing multi-line 
   comment marker `*/` - and place it at the top of your source 
   file.
  
2. Fill in the README section.

3. Specify any crates.io dependency in the `DEPS` variable. 
   Dependencies are space-separated. Example:
  
        DEPS="time toml libc";
  
4. Specify any git dependency in the `GIT_DEPS` variable. 
   Git dependencies are space-separated. The format of the 
   dependency specification is `CRATE-NAME|URL`. The following 
   example illustrates how one might specify two git dependencies:
  
        GIT_DEPS="$GIT_DEPS shams|https://bitbucket.org/joshmorin/shams.git";
        GIT_DEPS="$GIT_DEPS moid|https://bitbucket.org/joshmorin/moid.git";
  
5. Set the `DEV` variable to "Y" to avoid re-downloading 
   dependencies on each recompilation. This option is intended 
   for development only.
  
6. Set the executable bit for your source file.

7. At this point, your source file is now also a script.

8. Should you need to update dependencies, set the `UPDATE` 
   variable to "Y". This will force a recompile and run cargo 
   update. Note that you most likely will want to set the 
   variable at the command line rather than setting it in-file.



About
-----
This project was written by Josh Morin <JoshMorin@gmx.com>, 
and is made available under the CC0.


Code
----
This is a **valid** Rust source code file and a valid script:
    
    //usr/bin/env true;
    #![cfg_attr(DARF_V1, DARF_SCRIPT)] /*
    # --------------------------------- README -------------------------------------
    <<"INFORMATION BLOCK"

                                ..PROJECT README HERE..

    END OF THE
    INFORMATION BLOCK
    # ------------------------------ Configuration ---------------------------------

        # DEV               Developer mode: Reuses dir and deps. 
        # DEPS              Crates.io Dependencies
        # GIT_DEP           Git Dependencie
        # UPDATE            Updates dependencies. (Set at the command-line)
        
        DEV="Y"

    # ------------------------------------------------------------------------------
    set -e; DARF_VERSION="2"; CACHE="darf";
    ID=$([ "$DEV" ] && { echo -n dev-; echo "$0" | sha384sum; } || sha384sum "$0");
    ID=$(echo "$ID" | dd bs=1 count=20 2>/dev/null);
    DIR="$HOME/.$CACHE/$ID"; BIN="$DIR/target/release/$ID"; CARGO="$DIR/Cargo.toml"
    CARGO_HEADER='[package]\nname = "'"$ID"'"\nversion = "0.1.0"\nauthors = ["anon"]\n'
    tty -s && STDTTY=/dev/tty || STDTTY=/dev/NULL;
    # ------------------------------------------------------------------------------
    dep()       { echo "$1"' = "*"' >> "$CARGO"; }
    deps()      { [ "$DEPS" ] && { echo "[dependencies]" >> "$CARGO"; for i in $DEPS; do dep "$i"; done } || true; }
    git_dep()   { (IFS="|"; printf '[dependencies.%s]\ngit="%s"\n\n' $1) >> "$CARGO"; }
    git_deps()  { [ "$GIT_DEPS" ] && { for i in $GIT_DEPS; do git_dep "$i"; done } || true; }
    setup_src() { mkdir -p "$DIR/src"; cp "$0" "$DIR/src/main.rs"; }
    setup_crg() { mkdir -p "$DIR"; echo -e "$CARGO_HEADER" > "$CARGO"; deps; git_deps; }
    compile()   { cd "$DIR"; [ "$UPDATE" ] && cargo update; cargo build --release; cd - >/dev/null; }
    stop_root() { [ $(id -u) -eq 0 ] && echo "Can't build as root" 1>&2 && exit 1 || true; }
    build()     { stop_root; setup_crg; setup_src; compile; }
    # ------------------------------------------------------------------------------
    if ! [ \( -x "$BIN" \) -a \( "$0" -ot "$BIN" \) ] || [ "$UPDATE" ]; then 
        [ "$DEV" ] && echo "DEVELOPER MODE: $DIR" >$STDTTY
        build >$STDTTY 2>$STDTTY
        exec "$BIN" "$@"
    else
        exec "$BIN" "$@"
    fi
    # ----------------------------------------------------------------------------*/

    fn main(){
        println!("I Work!");
    }

